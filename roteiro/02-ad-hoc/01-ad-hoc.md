Ad hoc significa “para esta finalidade", “para isso” ou "para este efeito". É uma expressão latina, geralmente usada para informar que determinado acontecimento tem caráter temporário e que se destina para aquele fim específico.

Um exame ad hoc, um método ad hoc, um cargo ou uma função ad hoc, são exemplos que definem a criação de algo provisório, que vai atender apenas determinado propósito.

ADHOC no Ansible, São comandos executados diretamente na linha de comando com a instrução que deseja, vamos supor que você precise dar um start/stop/restart em um serviço em uma série de servidores, ou você precise saber quais servidores estão com a configuração X, ou ver espaço em disco, memória, ou talvez você queira instalar um pacote, enfim são N possibilidades, mais você só quer fazer isso em um dado momento e não necessariamente guardar essa configuração em playbook.

Parametro | Descrição
:---------|:---------
-i | Apresentar a localização do inventário
-u | Apresentar o usuário que executará o comando
-k | Solicitar a senha do usuário.
-m | Módulo que será utilizado.
-b | Substituir para o usuário root caso precisar.
-a | argumento do módulo.

O exemplo abaixo mostra que está rodando o comando com o usuário comum.
```
ansible ubuntu -u vagrant -k -m shell -a "id"
```

O exemplo abaixo mostra que está rodando o comando com o usuário root.
```
ansible ubuntu -u vagrant -k -b -m shell -a "id"
```
