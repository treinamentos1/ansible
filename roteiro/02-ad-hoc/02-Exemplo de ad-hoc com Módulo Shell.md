- Veja em **Commands modules**, o módulo [**Shell**](https://docs.ansible.com/ansible/2.9/modules/shell_module.html#shell inserir-module)

Exemplo mais simples do módulo **shell**, adicionando *-a* para inserir argumentos ou o comando puro.
```
ansible all -k -m shell -a "uptime"
```

Se precisarmos de privilégios de root, a opção -b nos promove como sudo.
```
ansible centos -k -m shell -a "id"
ansible centos -k -b -m shell -a "id"
```