Ansible não é uma linguagem de programação completa, mas possui vários recursos de linguagem de programação, e um dos mais importantes é a substituição de variáveis. Existem diferentes tipos de variáveis ​​disponíveis no Ansible.

# Criando nomes de variáveis ​​válidos
- Antes de começar a usar variáveis, é importante saber quais são os nomes de variáveis ​​válidos.
- O nome da variável deve incluir apenas letras, sublinhados e números — espaços não são permitidos.
- O nome da variável só pode começar com uma letra — eles podem conter números, mas não podem começar com um.
Por exemplo, os seguintes são bons nomes de variáveis:
```
http_port 
server_hostname_db1
```

No entanto, os exemplos a seguir são todos inválidos e não podem ser usados:
```
dbserver-east-zone 
app server ip 
web.server.port 
01dbserver
```

[Fonte](https://www.golinuxcloud.com/ansible-variables/)