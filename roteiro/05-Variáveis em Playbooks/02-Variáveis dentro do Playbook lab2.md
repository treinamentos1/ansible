Variáveis ​​definidas passando *-e* ou *--extra-vars* com *var=value* têm a precedência mais alta, o que significa que você pode usar isso para substituir variáveis ​​que já estão definidas.

Aqui eu tenho um playbook de exemplo *variable-with-cmd-line.yml* onde defini uma variável username com um valor como deepak. Estou então usando o módulo de depuração para imprimir o valor da username variável.   
Vamos criar o lab2:   
`mkdir lab2`

Agora vamos criar o Playbook:   
`mkdir lab2/ && vim lab2/playbook.yml`
```
---
 - hosts: localhost
   vars:
     username: neo
     password: matrix
   tasks:
   - name: Declarar Variavel
     debug:
       msg: "USUARIO={{ username }} SENHA={{ password }}"
```

Vamos executar o Playbook
```
ansible-playbook lab2/playbook.yml
```

[Fonte](https://www.golinuxcloud.com/ansible-variables/)