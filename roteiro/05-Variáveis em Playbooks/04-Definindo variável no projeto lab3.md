Se você estiver usando o playbook ansible em algum projeto, não é recomendável modificar o arquivo de inventário. Temos uma solução mais limpa disponível para definir variáveis ​​de host e grupo no Project.

Para definir a variável do host podemos criar um subdiretório host_vars e da mesma forma para definir a variável do grupo podemos criar um subdiretório group_vars dentro do diretório principal do projeto.

Por exemplo, vou criar um projeto "lab3".
```
mkdir lab3
```

Vamos criar o diretório host_vars
```
mkdir lab3/host_vars
```

Dentro de host_vars vamos criar um novo arquivo com o mesmo nome do servidor que queremos as variáveis, no nosso caso, ubuntu e definir as variáveis:   
`vim lab3/host_vars/ubuntu`
```
http_port : 8080
pkg : httpd
```

Vamos criar o arquivo do Playbook playbook.yml:   
`vim lab3/playbook.yml`
```
---
- hosts: ubuntu
  tasks:
  - name: Testando a primeira variável (http_port)
    debug:
      msg: "HTTP PORT é {{ http_port }}"
  - name: Testando a segunda variável (pkg)
    debug:
      msg: "O nome do pacote é {{ pkg }}"
```

Agora ao executar o Playbook, temos o resultado:   
`ansible-playbook lab3/playbook.yml`


[Fonte](https://www.golinuxcloud.com/ansible-variables/)