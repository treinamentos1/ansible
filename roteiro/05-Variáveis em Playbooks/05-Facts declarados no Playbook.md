---
 - name: Coletar IPv4 address da enp0s8
   hosts: ubuntu
   tasks:
   - debug:
       msg:
         - "Metodo-1 {{ ansible_enp0s8.ipv4.address }}"
         - "Metodo-2 {{ ansible_enp0s8['ipv4']['address'] }}"
         - "Metodo-3 {{ ansible_enp0s8['ipv4'].address }}"