Da mesma forma que o YAML, também podemos criar um arquivo de variáveis no formato Json.
Muitas automações podem vir como Json, por isso é interessante mencionarmos.   
`vim lab4/variaveis.json`

```
{
  "a": 50,
  "b": [1,2,3,4,5],
  "c": { "um":1, "dois":2 }
}
```

Em seguida, atualizaremos nosso arquivo de playbook para fazer referência ao arquivo de variáveis. Podemos colocar o caminho e o nome do arquivo variável usando uma linha separada e mencionar vários arquivos.   
`vim lab4/playbook.yml`
```
---
 - name: Collects variable from variables.yml file
   hosts: localhost
   vars_files:
   - variaveis.yml
   - variaveis.json
   gather_facts: false
   tasks:
   - debug:
       msg:
       - "Meu nome é {{ NOME }}"
       - "Este é o treinamento de {{ CURSO }}"
       - "Estamos falando sobre o capítulo {{ CAPITULO }}"
       - "O valor de b é: {{ b }}"
       - "O segundo valor de b é: {{ b[1] }}"
       - "O valor de c é {{ c }}"
```