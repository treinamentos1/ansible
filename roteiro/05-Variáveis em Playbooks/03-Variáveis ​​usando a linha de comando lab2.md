Variáveis ​​definidas passando *-e* ou *--extra-vars* têm a precedência mais alta, o que significa que você pode usar isso para substituir variáveis ​​que já estão definidas.

Aqui eu tenho um playbook de exemplo *lab2/playbook* onde definimos duas variáveis.      

Vamos executar o Playbook
```
ansible-playbook lab2/playbook.yml -e username=morpheus
```

[Fonte](https://www.golinuxcloud.com/ansible-variables/)