Criaremos um arquivo YAML separado variables.ymlcom o seguinte conteúdo:   

`mkdir lab4 && vim lab4/variaveis.yml`
```
NOME: Claudio
CURSO: Ansible
CAPITULO: Variaveis
```

Criaremos o arquivo do Playbook:   
`vim lab4/playbook.yml`

```
---
 - name: Collects variable from variables.yml file
   hosts: localhost
   vars_files: variaveis.yml
   gather_facts: false
   tasks:
   - debug:
       msg:
       - "Meu nome é {{ NOME }}"
       - "Este é o treinamento de {{ CURSO }}"
       - "Estamos falando sobre o capítulo {{ CAPITULO }}"
```

Vamos executar o playbook:   
`ansible-playbook lab4/playbook.yml`