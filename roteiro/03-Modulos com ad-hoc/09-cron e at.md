O módulo *at* Agenda a execução de um comando ou arquivo de script.

Agendamos para criar um arquivo.txt após um minuto: 
```
ansible all -m at -a "command='echo ola > /tmp/arquivo.txt' count=1 units=minutes"
```

Com o módulo *cron* podemos gerenciar entradas cron.d e crontab, segue exemplo de um comando cron:
![exemplo_cron](imagens/exemplo_cron.png)

O comando abaixo, criaremos um agendamento para upgrade a cada minuto.
```
ansible ubuntu -b -m cron -a "name='agendar upgrade' hour='1' user=root job='apt update && apt upgrade'"
ansible centos -b -m cron -a "name='agendar upgrade' hour='1' user=root job='yum upgrade -y'"
```

Fonte: [site do ansible1](https://docs.ansible.com/ansible/2.9/modules/at_module.html#at-module)
Fonte: [site do ansible2](https://docs.ansible.com/ansible/2.9/modules/cron_module.html#cron-module)