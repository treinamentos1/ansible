Com o argumento *chdir*, Mudando para um diretório antes de executar o comando.
```
ansible ubuntu -m shell -a "chdir=/tmp touch exemplo"
```
Argumento *creates*, se existir o arquivo mencionado, esta etapa não será executada.
```
ansible ubuntu -m shell -a "chdir=/tmp creates=exemplo touch exemplo2"
```

Fonte: [site do ansible](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/shell_module.html)