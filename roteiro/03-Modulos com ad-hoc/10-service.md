Com o módulo *service* Controlamos serviços. Os sistemas init suportados incluem SysV, systemd, upstart e outros.

No próximo exemplo manipularemos o serviço do daemon do ssh:
```
ansible all -b -m service -a "name=sshd state=started enabled=yes"
```

- **name=** Nome do serviço
- **state=started** Garante que o serviço, esteja ativo.
- **enabled=yes** Garante que o serviço se inicie junto com o sistema.

Fonte: [site do ansible1](https://docs.ansible.com/ansible/2.9/modules/service_module.html#service-module)