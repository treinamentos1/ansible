Clonar o repositório *Ansible Examples* para um local específico.

```
ansible all -m git -a "repo=https://github.com/ansible/ansible-examples.git dest=/tmp/ansible-examples"
```

Com o parâmetro *archive* podemos compactar o repositório assim que baixamos ele:
```
ansible all -m git -a "repo=https://github.com/ansible/ansible-examples.git dest=/tmp/ansible-examples archive=/tmp/ansible-examples.zip"
```

Com o parâmetro version, baixa e acessa uma branch específica.
```
ansible all -m git -a "repo=https://github.com/ansible/ansible-examples.git dest=/tmp/ansible-examples version=festdemo"
```


Fonte: [site do ansible](https://docs.ansible.com/ansible/2.9/modules/git_module.html#git-module)