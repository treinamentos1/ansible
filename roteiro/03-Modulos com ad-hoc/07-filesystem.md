Cria um sistema de arquivos.

O módulo *filesystem* cria um sistema de arquivos em um disco.

Etapas, crie um disco na VM Ubuntu.
- Desligar a VM
- Clicar em Configurações
- Ir até armazenamento
- em controladora SCSI, clicar em adicionar disco.
- Clicar em Criar
- Escolher VDI e continuar o processo.

Inicie a VM e ao acessar, execute o comando `lsblk -f` para verificar as discos e tipos de sistema de arquivos em cada um.

O novo disco *sdc* está sem sistema de arquivos.

Executar o módulo *filesystem* acrescentando o sistema de arquivos XFS.
```
ansible ubuntu -b -m filesystem -a "fstype=xfs dev=/dev/sdc"

```

Se precisarmos adicionar um novo sistema de arquivos, podemos forçar com o parâmetro *force*.
```
ansible ubuntu -b -m filesystem -a "fstype=ext4 dev=/dev/sdc force=yes"
```

Fonte: [site do ansible](https://docs.ansible.com/ansible/2.9/modules/filesystem_module.html#filesystem-module)