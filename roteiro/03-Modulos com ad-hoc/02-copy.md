No exemplo abaixo, criaremos o arquivo teste e enviaremos ao servidor ubuntu 
```
touch teste
ansible all -m copy -a "src=teste dest=/tmp"
```

Com o argumento *mode* adicionamos permissões específicas ao arquivo.
```
ansible all -m copy -a "src=teste dest=/tmp mode='u=rwx,g=r,o=r'"
ansible all -m copy -a "src=teste dest=/tmp mode=644"
```

Com o argumento *owner* e *group* Inserimos quem é dono e grupo do arquivo. O exemplo a seguir precisaremos do argumento -b para privilégios de root.
```
ansible all -b -m copy -a "src=teste dest=/tmp mode=700 owner=vagrant group=root"
```

Fonte: [site do ansible](https://docs.ansible.com/ansible/2.9/modules/copy_module.html#copy-module)