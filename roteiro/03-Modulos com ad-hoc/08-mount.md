Com o módulo *mount*, controlamos os pontos de montagem ativos e configurados.

```
ansible ubuntu -b -m mount -a "src=/dev/sdc path=/mnt/ fstype=ext4 state=mounted opts=rw,auto"
```

Com o parâmetro *state=mounted*, o dispositivo será montado ativamente e configurado apropriadamente no fstab.
Com o parâmetro *opts=* acrescentamos as opções de montagem, ex: (rw,auto) (ro,noauto)

Fonte: [site do ansible1](https://docs.ansible.com/ansible/2.9/modules/mount_module.html#mount-module)