Este módulo *timezone* configura a configuração do fuso horário, tanto do relógio do sistema quanto do relógio do hardware.

```
ansible all -b -m timezone -a "name='America/Recife'"
```

Para ver o resultado, o comando é `timedatectl`

Fonte: [site do ansible1](https://docs.ansible.com/ansible/2.9/modules/timezone_module.html#timezone-module)