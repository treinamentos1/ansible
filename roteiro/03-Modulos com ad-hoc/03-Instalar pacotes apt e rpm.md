Com o módulo *apt* instalamos um pacote na base Debian.
```
ansible ubuntu -b -m apt -a "name=cmatrix state=present update_cache=yes"
ansible ubuntu -b -m apt -a "name=cmatrix state=absent"
```

Com o módilo *yum* instalamos um pacote na base Red Hat.
```
ansible centos,oracle -b -m yum -a "name=telnet state=present"
ansible centos,oracle -b -m yum -a "name=telnet state=absent"
```

Com o módilo *package* instalamos um pacote na base Red Hat ou Debian.
```
ansible all -b -m package -a "name=git state=present"
```

Fonte1: [site do ansible](https://docs.ansible.com/ansible/2.9/modules/apt_module.html#apt-module)
Fonte2: [site do ansible](https://docs.ansible.com/ansible/2.9/modules/yum_module.html#yum-module)
Fonte3: [site do ansible](https://docs.ansible.com/ansible/2.9/modules/package_module.html#package-module)