### Pincipais parâmetros AD-HOC e Módulo shell

Módulos são pequenos programas que efetuam alguma ação no nó alvo, por exemplo ping, shell, powershell e outros.

- Veja página de [módulos.](https://docs.ansible.com/ansible/2.9/modules/modules_by_category.html)
