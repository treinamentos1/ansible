## Resumo do módulo
Vamos produzir o seguinte resultado neste módulo:
- Instalar o Nginx garantindo que está ativo e iniciando com o sistema;
- Garantir que o arquivo de configurações do Nginx esteja integro e fazer o deploy de uma página.
- Aprender a criar template baseado em variáveis dos facts, retornados pelo módulo *setup*
- Fazer o deploy do template.

### Playbook v1.   
Vamos criar um novo grupo no inventário:

`vim etc/ansible/hosts`
```
[proxy]
ubuntu
```

Vamos criar as tarefas do playbook, executando-as a cada vez que criar uma.

Para iniciar o nosso lab1/playbook.yml, inserimos o seguinte:
```
 ---           | indica o início do arquivo YAML, por boa prática adicionamos isto.
- hosts: proxy | Apresenta onde será executado o playbook
  become: yes  | tornamos usuário com privilégios
  tasks:       | Início da(s) tarefa(s), onde serão acrescentados os módulos e parâmetros.
  - name: Instalando o Proxy | Nome da tarefa
    apt:                | Módulo utilizado 
```


### Playbook v2

Vamos utilizar o módulo *setup* para reunir facts do servidor alvo e utilizarmos filtros.
Com estes filtros, podemos utilizar o resultado em variáveis, e criar um template em html. Por exemplo, nome do servidor, IP, versão do sistema operacional e etc.

Para o template funcionar, precisamos adicionar a extensão .j2. Jinja é um mecanismo de template da web para a linguagem de programação Python.
No exemplo abaixo, vamos utilizar o index.html.j2, inserindo facts como variáveis.

1. Executar o módulo em ad-hoc para coletar o que precisamos.
```
ansible proxy -m setup
```

2. Criar o template do lab1/index.html.j2

3. Substituir o módulo *copy* pelo *template* e substituir o arquivo *index.html* por *index.html.j2* no Playbook.



#### Fontes
- https://docs.ansible.com/ansible/latest/user_guide/become.html
- https://docs.ansible.com/ansible/2.9/modules/apt_module.html#apt-module
- https://docs.ansible.com/ansible/2.9/modules/service_module.html#service-module
- https://docs.ansible.com/ansible/2.9/modules/copy_module.html#copy-module
- https://docs.ansible.com/ansible/latest/user_guide/playbooks_handlers.html