Verifique se o Python está na versão 2 ou 3
```
python --version
```

Caso a versão seja Python 3.x, instale o pip com o seguinte comando:
```
apt -y install python3-pip
```

Caso for a versão Python 2.x execute o comando abaixo:
```
apt -y install python-pip
```

Após instalar o Pip, vamos executar a instalação das dependências para conexão com Windows. Se for Python 2.x utilize apenas **pip** se for Python 3.x utilize o comando **pip3**.
No exemplo abaixo está sendo utilizado pip para o Python versão 3.x

```
pip3 install setuptools_rust
pip3 install pywinrm
```

Feito isto, acesse o servidor Windows, quer seja com usuário de rede AD ou local.
Precisamos executar um script, haverá duas formas.
1. Poderá baixar o script e executar
```powershell
$url = "https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1"
$file = "\ConfigureRemotingForAnsible.ps1"
(New-Object -TypeName System.Net.WebClient).DownloadFile($url, $file)
powershell.exe -ExecutionPolicy ByPass -File $file
```

2. Caso for um servidor DMZ que não tem acesso à internet, abra a URL no navegador, copie e cole o conteúdo dentro do servidor.
[https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1](https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1)

Salve o arquivo em **C:\Windows\Temp\ansible.ps1**.

Após isto, execute o script como administrador no PowerShell.
```powershell
powershell.exe -ExecutionPolicy ByPass -File C:\Windows\Temp\ansible.ps1
```

Após isto, vamos alterar o arquivo de inventário. Subistitua o **user** e **password** de acordo com o seu ambiente.
```config
[windows]

[windows:vars]
ansible_user= xxxxxxxx
ansible_password= xxxxxxx
ansible_connection= winrm
ansible_port= 5986
ansible_winrm_server_cert_validation= ignore
ansible_winrm_scheme= https
ansible_winrm_transport= ntlm
```

Agora é hora de testar o Ansible
```shell
ansible windows -m win_shell -a 'powershell /c "dir"'
ansible windows -m win_shell -a 'cmd /c "dir"'
ansible windows -m win_command -a 'cmd /c "dir"'
ansible windows -m win_chocolatey -a "name=firefox state=present"
```
