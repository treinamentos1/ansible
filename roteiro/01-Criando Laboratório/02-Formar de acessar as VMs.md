Para acessar teremos dois métodos, via Vagrant ou via SSH.

1. Por Vagrant.

Comandos                | Descrição
:----------------------:| ---------------------------------------
`vagrant --help`          | Apresenta os comandos do Vagrant
`vagrant status`        | Verifica se VM estão ativas ou não.
`vagrant halt`          | Desliga as VMs
`vagrant up`            | Cria/Liga as VMs baseado no VagrantFile
`vagrant ssh <vm>`      | Acessa a VM
`vagrant ssh <vm> -c <comando>` | Executa comando via ssh
`vagrant reload <vm>`   | Reinicia a VM
`vagrant destroy <vm>`   | Remover a VM

Verifique quais VMs têm com `Vagrant status`, Acesse com `vagrant ssh NO-DA-VM`

2. Por SSH

Usuário e senha `vagrant`
```
ssh vagrant@IP
```

Para melhorar o processo de acesso SSH, podemos criar nomes no arquivo hosts.   
`vim /etc/hosts`
```
192.168.33.11   master
192.168.33.12   centos
192.168.33.13   oracle
192.168.33.14   ubuntu
```

Agora podemos acessar com o nome da VM.
```
ssh vagrant@NOME-DA-VM
```