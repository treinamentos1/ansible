Vamos criar relação de confiabilidade, uma chave ssh no *Master*, transferir para a máquina remota e em seguida validar que estamos conectando sem senha quando informamos a chave.
```
ssh-keygen -t rsa -f ansible -q -N ""
ssh-copy-id -i ansible.pub vagrant@centos
ssh-copy-id -i ansible.pub vagrant@oracle
ssh-copy-id -i ansible.pub vagrant@ubuntu
```

Após enviar as chaves ssh para os servidores, apresentamos a chave ao invés de senha:
1. Apresentando a chave SSH:
```
ansible -i hosts linux --private-key ansible -m ping
```
```
ssh -i ansible vagrant@centos
```

2. Apresentando a chave SSH ao arquivo ansible.cfg:

`sudo vim /etc/ansible/ansible.cfg`

```
private_key_file=/home/vagrant/.ssh/ansible
```

Após inserir a chave conforme acima, garantimos o acesso sem solicitar senha ou chave ssh.
```
ansible -i hosts deb -m ping
```