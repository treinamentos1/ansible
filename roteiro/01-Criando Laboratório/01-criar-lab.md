### Preparando o ambiente

- [Instalar Git](https://gitlab.com/treinamentos1/ansible/-/blob/main/instalacoes/01-git.sh)
- [instalar VirtualBox](https://gitlab.com/treinamentos1/ansible/-/blob/main/instalacoes/01-git.sh)
- [Instalar o Vagrant](https://gitlab.com/treinamentos1/ansible/-/blob/main/instalacoes/03-vagrant.sh)
- Criar as Máquinas virtuais no Vagrant, executando o comando abaixo onde estiver o arquivo [Vagrantfile](https://gitlab.com/treinamentos1/ansible/-/blob/main/instalacoes/Vagrantfile)
```
vagrant up
```

_O Laboratório **pode demorar**, dependendo da conexão de internet e poder computacional, para ficar totalmente preparado._

Nesse laboratório, que está centralizado no arquivo Vagrantfile, serão criadas as máquinas com as seguintes características:

Nome       | vCPUs | Memoria RAM | IP            | S.O.¹           
---------- |:-----:|:-----------:|:-------------:|:---------------:
Master     | 1     | 512MB | 192.168.33.11 | focal 20.04
Centos     | 1     | 512MB | 192.168.33.12 | 7
Oracle     | 1     | 512MB | 192.168.33.13 | 8
Ubuntu     | 1     | 512MB | 192.168.33.14 | focal 20.04 