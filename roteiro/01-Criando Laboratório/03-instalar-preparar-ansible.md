### Instalar Ansible

Acessamos a máquina Master
```
vagrant ssh master
```

Na máquina virtual Master, instalar o Ansible com o [script](https://gitlab.com/treinamentos1/ansible/-/blob/main/instalacoes/install-ansible.sh) ou manualmente com a fonte.

- Mostrar e explicar `ansible --version`
- Mostrar e explicar o diretório `/etc/ansible/`
