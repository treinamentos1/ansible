Anteriormente precisamos mencionar o IP do servidor, em uma infraestrutura, geralmente damos nomes ao servidores para comunicar-se com eles.

### 1. método de inserão do hostname no arquivo de inventário `hosts`, utilizado no Ansible.
`vim hosts`
```
centos ansible_ssh_host=192.168.33.12
oracle ansible_ssh_host=192.168.33.13
ubuntu ansible_ssh_host=192.168.33.14
```

Se executarmos o módulo ping do ansible adicionando o hostname, funcionará:
```
ansible -i hosts centos -k -m ping
```

### 2. método via arquivo `hosts` do servidor master.
`sudo vim /etc/hosts`
```
192.168.33.12   centos
192.168.33.13   oracle
192.168.33.14   ubuntu
```

Ajustamos nosso arquivo de inventário do Ansible que chamamos de `hosts`, adicionando o hostname.
`vim ~/ansible/hosts`
```
centos
oracle
ubuntu
```

Se efetuarmos um ping normal ou com o módulo do Ansible com o hostname que adicionamos a cada um IP, dará certo.
```
ansible -i hosts all -k -m ping
```

### Ajustando grupos de host

Vamos criar dois grupos e um grupo filho:
```
[rpm]
centos
oracle

[deb]
ubuntu

[linux:children]
rpm
deb
```

### Opções interessandos

Se tivéssemos uma sequência de servidores, poderíamos utilizar da seguinte forma:
```
[db]
servidor[1:10]
```
vai corresponder server1, server2, server3, ...server10


Se desejasse inserir senha em um ou alguns servidores:
```
[password]
centos ansible_ssh_user=vagrant ansible_ssh_pass=vagrant
```
Ao executar o comando abaixo, não pedirá senha:
```
ansible -i hosts ubuntu -m shell -a "uptime"
```


Se desejasse inserir senha em um grupo de servidores:
```
[linux:vars]
ansible_ssh_user=vagrant
ansible_ssh_pass=vagrant
```
Ao executar o comando abaixo, não pedirá senha:
```
ansible -i hosts ubuntu -m shell -a "uptime"
```

Fonte1: [site do ansible](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html)
Fonte2: [site](https://www.golinuxcloud.com/ansible-inventory-files/)