Para concluir o módulo, vamos enviar nosso Inventário para o arquivo padrão, assim não precisamos repetir a sintaxe -i hosts.   
`vim /etc/ansible/hosts`
```
[rpm]
centos
oracle

[deb]
ubuntu

[linux:children]
rpm
deb

[linux:vars]
ansible_ssh_user=vagrant
ansible_ssh_pass=vagrant
```

