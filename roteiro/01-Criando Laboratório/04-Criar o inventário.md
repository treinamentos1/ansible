### Criar o inventário

O Ansible funciona usando uma lista ou grupo de listas conhecido como inventário. O local padrão do inventário é um arquivo chamado /etc/ansible/hosts mas podemos personalizar onde deixar e utilizar o host.

 - Criar diretório *ansible* no path do usuário
```
mkdir ansible && cd ansible
```

Criar o arquivo hosts e inserir no arquivo os IP's do lab:   
`vim hosts`
```
192.168.33.12
192.168.33.13
192.168.33.14

```
- Se executarmos um comando ad-hoc neste momento, dará um erro, indica que precisa da chave de confiabilidade.
```
ansible -i hosts 192.168.33.12 -k -m ping
```

Precisamos ajustar o arquivo de configuração do Ansible especificar que não cheque a chave de confiabilidade.
`sudo vim /etc/ansible/ansible.cfg`
```
[defaults]
host_key_checking = false
```

Agora, com as mudanças feitas, podemos executar o modulo ping corretamente.
```
ansible -i hosts all -m ping
ansible -i hosts 192.168.33.12 -k -m ping
```

Fonte1: [site do ansible](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html)